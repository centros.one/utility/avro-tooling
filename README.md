# Avro Tooling

This repository contains a command line tool used for working with Avro files.

## Usage

The easiest way to use the tool is by running the provided Docker image:

    docker run -it --rm registry.gitlab.com/centros.one/utility/avro-tooling:latest help

This will print the available commands that can be used. Normally you will want to read
some file from the local filesystem and perform some action on that. You can do this by
mounting the current working directory as a volume in the Docker container:

    docker run --rm -ti -v `pwd`:/mnt registry.gitlab.com/centros.one/utility/avro-tooling:latest help

This will make all files in the current folder available under `/mnt` in the Docker Container.

### Configure logging

When running the Docker image logging can be configured by setting one of the SimpleLogger [config properties](http://www.slf4j.org/api/org/slf4j/impl/SimpleLogger.html):

    docker run --rm -ti -v `pwd`:/mnt --entrypoint java registry.gitlab.com/centros.one/utility/avro-tooling:latest -Dorg.slf4j.simpleLogger.defaultLogLevel=error -jar /opt/app.jar help

**Note:** You have to override the container entrypoint to be able to set the java property before
specifying the jar that is to be executed.

## Building locally

If you don't want to use the Docker image you can also clone the repository and build the
tool locally.

### Prerequisites

- Java 8+
- Maven 3.6+

### Usage

Execute the following command in the root of the repository:

    mvn clean package

This will build a JAR containing all dependencies needed for executing the tool and save it
to `target/avro-tooling-${version}-shaded.jar`.

Execute it with

    java -jar /path/to/avro-tooling-${version}-shaded.jar
