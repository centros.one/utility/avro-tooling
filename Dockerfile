FROM maven:3.6-jdk-11 as BUILD

COPY . /usr/src/app
RUN mvn --batch-mode -f /usr/src/app/pom.xml clean package

FROM openjdk:11-jdk
COPY --from=BUILD /usr/src/app/target/*-shaded.jar /opt/app.jar
WORKDIR /opt

ARG COMMIT_SHA
ENV COMMIT_SHA ${COMMIT_SHA}
ARG COMMIT_REF_SLUG
ENV COMMIT_REF_SLUG ${COMMIT_REF_SLUG}

ENTRYPOINT ["java", "-jar", "/opt/app.jar"]
