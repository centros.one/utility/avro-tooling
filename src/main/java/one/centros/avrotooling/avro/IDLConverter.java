package one.centros.avrotooling.avro;

import org.apache.avro.Protocol;
import org.apache.avro.Schema;
import org.apache.avro.compiler.idl.Idl;
import org.apache.avro.compiler.idl.ParseException;

import java.io.File;
import java.io.IOException;

public class IDLConverter {

    private String protocolSuffix;

    public IDLConverter(String protocolSuffix) {
        this.protocolSuffix = protocolSuffix;
    }

    public String convert(File file) throws IOException, ParseException {
        Idl parser = new Idl(file);

        Protocol protocol = parser.CompilationUnit();
        Object versionProp = protocol.getObjectProp("version");
        Long version = null;
        if (versionProp instanceof Long) {
            version = (Long) versionProp;
        }
        String basename = removeSuffix(protocol.getName(), protocolSuffix);
        String result = null;
        for (Schema schema : protocol.getTypes()) {
            if (schema.getName().equals(basename)) {
                if (version != null) {
                    schema.addProp("version", version);
                }
                result = schema.toString(true);
                break;
            }
        }
        parser.close();
        return result;
    }

    private String removeSuffix(String string, String suffix) {
        int lastIndex = string.lastIndexOf(suffix);
        if (lastIndex > -1) {
            return string.substring(0, lastIndex);
        }
        return string;
    }
}
