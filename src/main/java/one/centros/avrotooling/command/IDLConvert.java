package one.centros.avrotooling.command;

import one.centros.avrotooling.avro.IDLConverter;
import org.apache.avro.compiler.idl.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.Callable;

@CommandLine.Command(name="idl-convert", description = "converts an Avro IDL specification to a single Avro schema file")
public class IDLConvert implements Callable<Integer> {
    @CommandLine.Option(names = "-s", description = "suffix for protocol name that will be stripped to determine main record in protocol")
    String protocolSuffix = "Protocol";

    @CommandLine.Parameters(paramLabel = "FILE", description = "the Avro IDL file to convert")
    File inputFile;

    @CommandLine.Parameters(paramLabel = "OUTPUT", description = "the output filename (use \"-\" for stdout)")
    String outputPath;

    public static final Logger LOGGER = LoggerFactory.getLogger(IDLConvert.class);

    @Override
    public Integer call() {
        LOGGER.info("Converting file: " + inputFile.getAbsolutePath());
        IDLConverter converter = new IDLConverter(protocolSuffix);
        String result;
        try {
            result = converter.convert(inputFile);
        } catch (IOException e) {
            LOGGER.error("Could not read input file: " + e.getMessage());
            return 1;
        } catch (ParseException e) {
            LOGGER.error("Could not parse IDL: " + e.getMessage());
            return 2;
        }

        if (result == null) {
            LOGGER.error("Could not find main record in IDL");
            return 4;
        }

        LOGGER.info("Writing output to \"" + outputPath + "\"");
        if (outputPath.equals("-")) {
            System.out.println(result);
        } else {
            try {
                FileWriter fileWriter = new FileWriter(outputPath);
                PrintWriter printWriter = new PrintWriter(fileWriter);
                printWriter.println(result);
                printWriter.close();
            } catch (IOException e) {
                LOGGER.error("Could not write to output file: " + e.getMessage());
                return 3;
            }
        }

        return 0;
    }
}
