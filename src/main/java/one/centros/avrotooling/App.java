package one.centros.avrotooling;

import one.centros.avrotooling.command.IDLConvert;
import picocli.CommandLine;


@CommandLine.Command(name = "AvroTooling", subcommands = {IDLConvert.class, CommandLine.HelpCommand.class})
public class App {
    public static void main(String[] args) {
        int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);
    }
}
