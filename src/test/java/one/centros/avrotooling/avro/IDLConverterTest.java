package one.centros.avrotooling.avro;

import org.apache.avro.compiler.idl.ParseException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import static junit.framework.TestCase.assertEquals;

public class IDLConverterTest {

    public static String PROTOCOL_SUFFIX = "Protocol";

    @Test
    public void shouldConvertSimpleIdl() throws IOException, ParseException {
        IDLConverter converter = getIdlConverter();
        String result = converter.convert(loadResource("simple.avdl"));
        assertEquals("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"CompanyRecord\",\n" +
                "  \"namespace\" : \"one.centros.avrotooling.avro-tooling\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"name\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"countryCode\",\n" +
                "    \"type\" : \"string\"\n" +
                "  } ]\n" +
                "}", result);
    }

    @Test
    public void shouldConvertIdlWithMultipleRecords() throws IOException, ParseException {
        IDLConverter converter = getIdlConverter();
        String result = converter.convert(loadResource("multiple.avdl"));
        assertEquals("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"CompanyRecord\",\n" +
                "  \"namespace\" : \"one.centros.avrotooling.avro-tooling\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"name\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"countryCode\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"user\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"UserRecord\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"name\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"age\",\n" +
                "        \"type\" : \"int\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  } ]\n" +
                "}", result);
    }

    @Test
    public void shouldConvertIdlWithImport() throws IOException, ParseException {
        IDLConverter converter = getIdlConverter();
        String result = converter.convert(loadResource("import.avdl"));
        assertEquals("{\n" +
                "  \"type\" : \"record\",\n" +
                "  \"name\" : \"ProjectRecord\",\n" +
                "  \"namespace\" : \"one.centros.avrotooling.avro-tooling\",\n" +
                "  \"fields\" : [ {\n" +
                "    \"name\" : \"name\",\n" +
                "    \"type\" : \"string\"\n" +
                "  }, {\n" +
                "    \"name\" : \"company\",\n" +
                "    \"type\" : {\n" +
                "      \"type\" : \"record\",\n" +
                "      \"name\" : \"CompanyRecord\",\n" +
                "      \"fields\" : [ {\n" +
                "        \"name\" : \"name\",\n" +
                "        \"type\" : \"string\"\n" +
                "      }, {\n" +
                "        \"name\" : \"countryCode\",\n" +
                "        \"type\" : \"string\"\n" +
                "      } ]\n" +
                "    }\n" +
                "  } ]\n" +
                "}", result);
    }

    private IDLConverter getIdlConverter() {
        IDLConverter converter = new IDLConverter(PROTOCOL_SUFFIX);
        return converter;
    }

    private File loadResource(String resource) {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(Objects.requireNonNull(classLoader.getResource(resource)).getFile());
        return file;
    }
}
